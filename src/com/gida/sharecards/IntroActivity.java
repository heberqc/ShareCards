package com.gida.sharecards;

import com.gida.sharecards.R;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

public class IntroActivity extends Activity {
	CheckBox chck;
	SharedPreferences pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_intro);
		pref = getSharedPreferences("config", Context.MODE_PRIVATE);

		chck = (CheckBox) findViewById(R.id.checkBox);
		chck.setChecked(pref.getBoolean("show", false));

	}

	// luego de cargar todo el layout, al hacer clic en el botOn siguiente se
	// ejecuta este mEtodo que guardarA en SharedPreferences el valor del check
	// que pregunta si se desea que la prOxima vez se muestre la introducciOn
	public void onClick(View v) {
		Editor editor = pref.edit();
		editor.putBoolean("show", chck.isChecked());
		editor.commit();
		this.finish();
	}
}
