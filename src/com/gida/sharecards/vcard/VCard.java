package com.gida.sharecards.vcard;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import android.os.Environment;
import android.util.Log;
import android.content.ContextWrapper;

import com.gida.sharecards.bean.DatosUsuario;

public class VCard {

	// ATRIBUTOS

	// Datos fijos que se usarAn en la creaciOn de un DatosUsuario a vCard
	private final static String inicio = "BEGIN:VCARD";
	private final static String version = "VERSION:2.1";
	private final static String fin = "END:VCARD";
	private final static String endl = "\n"; // salto de lInea
	private final static String sc = ";"; // punto y coma
	private final static String[] campos = { "N:", "FN:", "TEL;CELL:", "NOTE:" };

	// METODOS

	// Getters and Setters

	// obtenciOn del N de un archivo vCard donde es necesario separar los
	// nombres es caso el DatosUsuario tenga dos nombres
	public static String getN(DatosUsuario datos) {
		if (!(datos.getName().length() == 0 && datos.getSurname().length() == 0)) {
			String priNom = ""; // primer nombre
			String secNom = ""; // segundo nombre
			short n = (short) datos.getName().length();
			Boolean variosNombres = false;
			short i;
			char aux[] = new char[n];
			char letra;
			// capturando el primer nombre
			for (i = 0; i < n; i++) {
				letra = datos.getName().charAt(i);
				if (letra == ' ') {
					variosNombres = true;
					break;
				} else {
					aux[i] = letra;
				}
			}
			priNom = String.valueOf(aux).trim();
			// capturando el segundo nombre
			if (variosNombres) {
				short k = 0;
				i++;
				aux = new char[n];
				while (i < n) {
					aux[k] = datos.getName().charAt(i);
					i++;
					k++;
				}
				secNom = String.valueOf(aux).trim();
				return datos.getSurname() + sc + priNom + sc + secNom + sc + sc;
			} else {
				return datos.getSurname() + sc + priNom.trim() + sc + sc + sc;
			}
		} else { // si nombres y apellidos estAn vacIos
			return sc + sc + sc + sc;
		}
	}

	// generaciOn del texto que se escribirA en el archivo
	public static String toStringVcf(DatosUsuario datos) {
		return inicio + endl + version + endl + campos[0] + getN(datos) + endl
				+ campos[1] + datos.getFN() + endl + campos[2]
				+ datos.getHomeCell() + endl + campos[3] + datos.getNotes()
				+ endl + fin;
	}

	// retorna el archivo (.vcf - vCard) creado
	public static File toFile(File dir, DatosUsuario datos) {
		File vcfFile = new File(dir, datos.getFN() + ".vcf");
		FileWriter fw;
		try {
			fw = new FileWriter(vcfFile);
			fw.write(toStringVcf(datos));
			fw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (vcfFile.exists() && vcfFile.canRead()) {
			return vcfFile;
		} else {
			return null;
		}

	}

	// Obtiene la direccion donde se almacenaran los archivos
	public static File getDir(ContextWrapper contextWrapper) {
		File dir = null;
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			dir = contextWrapper.getExternalFilesDir(null);
			Log.i("VCard.getDir", "Hay una micro sd : " + dir.getAbsolutePath());
		} else {
			dir = contextWrapper.getFilesDir();
			Log.i("VCard.getDir",
					"No hay una micro sd : " + dir.getAbsolutePath());
		}
		return dir;
	}

}
