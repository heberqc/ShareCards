package com.gida.sharecards.adapters;

import java.util.List;
import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gida.sharecards.R;
import com.gida.sharecards.bean.DatosUsuario;

public class ItemCardAdapter extends ArrayAdapter<DatosUsuario> {

	Activity context;
	List<DatosUsuario> values;

	// constructor del adaptador, como parAmetros de entrada se recibe un
	// context y un List<DatosUsuario>
	public ItemCardAdapter(Activity context, List<DatosUsuario> values) {
		super(context, R.layout.item_card, values);
		this.context = context;
		this.values = values;
	}

	// este mEtodo retorna el Item que se mostrarA en el ListView
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = context.getLayoutInflater();
		// vista que serA Item de la lista que se mostrarA en el MainActivity
		View item = inflater.inflate(R.layout.item_card, null);

		// introduciOn de los datos en el Item
		TextView tvCardName = (TextView) item.findViewById(R.id.tvCardName);
		tvCardName.setText(values.get(position).getCardName());

		TextView tvCardNotes = (TextView) item.findViewById(R.id.tvName);
		tvCardNotes.setText(values.get(position).getNotes());

		// si no hay notas, se repite el nombre de la tarjeta como nota
		if (tvCardNotes.getText().length() == 0) {
			tvCardNotes.setText(tvCardName.getText());
		}

		return (item);
	}

	// el List<DatosUsuario> se recibe con un mEtodo de la clase padre
	// ArrayAdapter<DatosUsuario>, es por eso que no es necesario crear un
	// mEtodo para eso
} // fin
