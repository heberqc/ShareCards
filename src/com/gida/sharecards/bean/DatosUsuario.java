package com.gida.sharecards.bean;

import java.io.Serializable;

public class DatosUsuario implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String CLASS_NAME = "DATOS_USUARIO";
	private long id;
	private String cardName;
	private String name;
	private String surname;
	private String homeCell;
	private String notes;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCardName() {
		return cardName;
	}

	public void setCardName(String cardName) {
		this.cardName = cardName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getHomeCell() {
		return homeCell;
	}

	public void setHomeCell(String homeCell) {
		this.homeCell = homeCell;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	// este mEtodo devuelve el FN que se usarA para el nombre del vCard y para
	// llenar unos de los campos del vCard (FN)
	public String getFN() {
		return (name + " " + surname).trim();

	}

}
