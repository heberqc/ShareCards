package com.gida.sharecards.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteOpenHelper extends SQLiteOpenHelper {

	// definiciOn de la base de datos y la tabla donde se guardan las tarjetas
	public static final String TABLE_CARDS_INFO = "info_cards";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_CARD_NAME = "card_name";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_SURNAME = "surname";
	public static final String COLUMN_HOME_CELL = "home_cell";
	public static final String COLUMN_NOTES = "notes";
	private static final String DATABASE_NAME = "database_vcards.db";
	private static final int DATABASE_VERSION = 1;

	// SQL Statement
	private static final String DATABASE_CREATE = "create table "
			+ TABLE_CARDS_INFO + "(" + COLUMN_ID
			+ " integer primary key autoincrement, " + COLUMN_CARD_NAME
			+ " text not null," + COLUMN_NAME + " text," + COLUMN_SURNAME
			+ " text," + COLUMN_HOME_CELL + " text," + COLUMN_NOTES + " text);";

	public MySQLiteOpenHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteOpenHelper.class.getName(),
				"Actualizando Base de datos de la versi�n: " + oldVersion
						+ " a " + newVersion + ".");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_CARDS_INFO);
		onCreate(db);
	}

}
