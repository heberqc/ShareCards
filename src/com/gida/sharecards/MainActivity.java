package com.gida.sharecards;

import java.io.File;
import java.util.List;

import com.gida.sharecards.adapters.ItemCardAdapter;
import com.gida.sharecards.bean.DatosUsuario;
import com.gida.sharecards.database.CardsDataSource;
import com.gida.sharecards.vcard.VCard;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.net.Uri;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.view.View;
import android.widget.Toast;

@SuppressLint("DefaultLocale")
public class MainActivity extends Activity {

	public static final int NEW_CARD = 1;
	public static final int UPDATE_CARD = 0;
	public static final int DELETE_CARD = -1;
	public static final int OTHER = 2;

	public ListView lv_listCards; // ListView donde se mostrarAn las tarjetas
	private List<DatosUsuario> listDatosUsuario; // values es un List de Datos
													// usuario
	private CardsDataSource dataSource; // comunicaciOn con la base de datos
	private ItemCardAdapter adaptador; // adapatador de listDatosUsuario -->
	private File dirVCardFiles; // directorio de los archivos vcf se guardaran
								// mientras la aplicaciOn funcione

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("ShareCards v0.68.20130925", "INICIO DE LA APLICACI�N");
		setTitle(R.string.title_activity_main);

		// verificando si se debe presentar la introducciOn
		SharedPreferences preferences = getSharedPreferences("config",
				Context.MODE_PRIVATE);
		Boolean check = preferences.getBoolean("show", false);
		if (!check.booleanValue()) {
			Intent i = new Intent(getApplicationContext(), IntroActivity.class);
			startActivity(i);
		}
		setContentView(R.layout.activity_main);

		// direcciOn en la cual se almacenarAn las tarjetas
		dirVCardFiles = VCard.getDir(this);
		Log.i("Directorio vCard", dirVCardFiles.getAbsolutePath());

		// ListView donde se mostrarAn las tarjeta, este es el componente
		// grAfico
		lv_listCards = (ListView) findViewById(R.id.listViewCards);

		// apertura de la base de datos
		dataSource = new CardsDataSource(getApplicationContext());

		// obtener los datos de la base de datos para cargar la lista de
		// Tarjetas y el listView
		loadList(this);

		// acciOn al hacer un clic en un Item de la lista
		lv_listCards.setOnItemClickListener(new OnItemClickListener() {

			// onItemClic recibe automAtiamente los parAmetros de posiciOn donde
			// se hizo clic
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				// creaci�n de la tarjeta para compartir
				File tarjeta = VCard.toFile(dirVCardFiles,
						listDatosUsuario.get(position));

				// implementado el envIo por Intent
				Intent intent = new Intent(Intent.ACTION_SEND);
				intent.setType("text/x-vcard");
				// intent.putExtra(Intent.EXTRA_EMAIL, new String[]
				// {"email@example.com"});
				intent.putExtra(Intent.EXTRA_SUBJECT, "vCard "
						+ listDatosUsuario.get(position).getFN());
				intent.putExtra(Intent.EXTRA_TEXT,
						getResources().getString(R.string.contact) + " "
								+ listDatosUsuario.get(position).getFN());

				// comprobaciOn, la primera condiciOn del if es el de 'si se
				// creO el archivo correctamente' con los datos de la direcciOn
				// y el DatosUsuario de donde se obtienen los datos.
				// En caso de error se muestra el Toast y se termina el mEtodo
				if (tarjeta == null) {
					Toast.makeText(
							getApplicationContext(),
							getResources().getString(R.string.error_attachment),
							Toast.LENGTH_SHORT).show();
					return;
				}

				// continuando con la adjuntaciOn del archivo al Intent
				Uri uri = Uri.parse("file://" + tarjeta);
				intent.putExtra(Intent.EXTRA_STREAM, uri);

				// lanzamiento del Intent para compartir
				startActivity(Intent.createChooser(intent, getResources()
						.getString(R.string.share)
						+ " "
						+ getResources().getString(R.string.card)
						+ " "
						+ getResources().getString(R.string.using) + "..."));
			}
		});

		// mEtodo de clic largo en un Item del ListView
		lv_listCards.setOnItemLongClickListener(new OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view,
					int position, long id) {

				Intent intnt_create = new Intent(getApplicationContext(),
						CreateCardActivity.class);
				intnt_create.putExtra(DatosUsuario.CLASS_NAME,
						listDatosUsuario.get(position));
				startActivityForResult(intnt_create, UPDATE_CARD);
				return false;
			}
		});

	}

	// m�todo que carga la lista de Tarjeta con los datos de la base de datos,
	// luego actualiza el listView
	private void loadList(Context context) {
		dataSource.open();
		listDatosUsuario = dataSource.getAllComments();
		adaptador = new ItemCardAdapter(this, listDatosUsuario);
		lv_listCards.setAdapter(adaptador);
		dataSource.close();

	}

	// carga del menU
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	// acciones de las opciones del menU: nueva tarjeta y ver introducciOn
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {

		// observar que el Intent de este caso se inicia de la forma
		// startActivityForResult y no de la forma startActivity comUn en otros
		// casos
		case R.id.action_new:
			Intent newStudentIntent = new Intent(getApplicationContext(),
					CreateCardActivity.class);
			startActivityForResult(newStudentIntent, NEW_CARD);
			break;

		case R.id.action_show_intro:
			Intent i = new Intent(getApplicationContext(), IntroActivity.class);
			startActivity(i);
			break;

		case R.id.action_settings:
			Toast.makeText(this,
					getResources().getString(R.string.action_settings),
					Toast.LENGTH_SHORT).show();
			break;

		default:
			return super.onOptionsItemSelected(item);
		}

		return true;
	}

	// mEtodo de aniadir una nueva tarjeta a la base de datos
	private void addCard(DatosUsuario datos) {
		dataSource.open();
		dataSource.addCard(datos);
		loadList(this);
	}

	// mEtodo de actualizaciOn de datos de un registro de la base de datos
	private void updateCard(DatosUsuario datos) {
		dataSource.open();
		dataSource.updateCard(datos);
		loadList(this);
	}

	// mEtodo de eliminaciOn de un registro de la base de datos
	private void deleteCard(DatosUsuario datos) {
		dataSource.open();
		dataSource.deleteCard(datos);
		loadList(this);
		Log.i("PROCESO DE ELIMINACION", "Se elimin� la tarjeta");
	}

	// mEtodo que recibe del startActivityForResult
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {

		DatosUsuario temp = (DatosUsuario) data
				.getSerializableExtra(DatosUsuario.CLASS_NAME);

		// caso del cOdigo que fue enviado
		switch (requestCode) {
		case NEW_CARD:
			if (resultCode == OTHER) {
				break;
			} else {
				Log.i("RESULT CODE", "Se agrega una nueva tarjeta");
				addCard(temp);
			}
			break;
		case UPDATE_CARD:
			// caso de la respuesta
			switch (resultCode) {
			case UPDATE_CARD: // actualizar
				Log.i("RESULT CODE", "Se actualizar� una tarjeta");
				updateCard(temp);
				break;
			case DELETE_CARD: // eliminar tarjeta
				Log.i("RESULT CODE", "Se eliminar� una tarjeta");
				deleteCard(temp);
				break;
			default:
				break;
			}
			break;
		default:
			break;
		}
	}

	@Override
	protected void onResume() {
		dataSource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		dataSource.close();
		super.onPause();
	}

	// mEtodo de cierre de la aplicaciOn
	@Override
	protected void onDestroy() {
		// Este m�todo limpiar� la memoria donde se hayan creado los vCards

		// Directory de los vCards que se crearon
		String path = dirVCardFiles.getAbsolutePath();

		String files;
		File folder = new File(path);
		File[] listOfFiles = folder.listFiles();

		for (int i = 0; i < listOfFiles.length; i++) {
			files = listOfFiles[i].getName();
			Log.i("Limpieza de la memoria", "Se elimina " + files);
			new File(path, files).delete();
		}
		Log.i("ShareCards v0.68.20130925", "FIN DE LA APLICACI�N");
		super.onDestroy();
	}

}
